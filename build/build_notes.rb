#!/usr/bin/env ruby
# encoding: UTF-8
# coding: UTF-8

require 'fileutils'

Encoding.default_internal = Encoding::UTF_8

# Función que crea los diferentes formatos
def build md
    puts "===> Trabajando con «#{File.basename(md)}»…\n\n"

    file_number = 0
    file_name = ''
    file_title = ''
    new_md = []
    new_html = []

    # Crea proyecto de automata
    system("pc-automata --init")
    Dir.chdir('epub-automata')

    # Obtiene los metadatos
    file = File.open(md, 'r:UTF-8')
    file.each_with_index do |linea, i|
        if linea =~ /^#\s+/ && i == 2
            file_name = File.basename(md, '.md')
            file_title = linea.gsub(/^#\s+/, '').strip
            file_number = File.basename(md, '.md').to_i
            $notes.push({:number => file_number, :name => file_name, :title => file_title})
        end

        new_md.push(linea.gsub('../img', '../img/' + file_name))
    end
    file.close

    # Recrea los metadatos
    file = File.new('meta-data.yaml', 'w:UTF-8')
    file.puts $meta_data.gsub('$$name$$', file_name).gsub('$$title$$', '"' + file_title + '"').gsub('$$number$$', file_number.to_s)
    file.close

    # Crea los files
    system("pc-automata -f ../md/#{file_name}.md -c ../covers/#{file_name}.png -i ../img/#{file_name} --depth 2 --section --no-ace --no-analytics --overwrite")

    Dir.glob('*.{epub,mobi}') do |file|
        if File.extname(file) == '.epub' && File.basename(file) =~ /3.0.0/ || File.extname(file) == '.mobi'
            FileUtils.mv(file, "../#{File.extname(file)[1..-1]}/#{file_name + File.extname(file)}")
        end
    end

    # Va a la carpeta de MD para crear los HTML
    Dir.chdir('../md')

    # Crea el nuevo file MD con los enlaces a las imágenes correctos
    file = File.new(file_name + '_tmp.md', 'w:UTF-8')
    file.puts new_md
    file.close

    # Genera el HTML
    system("pc-pandog -i #{file_name}_tmp.md -o #{file_name}.html")

    # Cambia el título del documento HTML
    file = File.open(file_name + '.html', 'r:UTF-8')
    file.each do |linea|
        new_html.push(linea
          .gsub('<title>Título</title>', "<title>#{file_title}</title>")
          .gsub('    </body>', "        <section>\n        </section>\n    </body>"))
    end
    file.close
    file = File.new(file_name + '.html', 'w:UTF-8')
    file.puts new_html
    file.close

    # Elimina el MD temporal y mueve el HTML a la carpeta correspondiente
    FileUtils.rm(file_name + '_tmp.md')
    FileUtils.mv(file_name + '.html', "../html/#{file_name}.html")

    # Elimina protecto de automata
    Dir.chdir('..')
    FileUtils.rm_rf('epub-automata')
end

# Variables
$notes = []
$meta_data = "---\n# Generales\ntitle: $$title$$\nsubtitle: Nota $$number$$ del Taller de Edición Digital\nauthor:\n  - Zhenya, Nika\npublisher:\n  - Perro Tuerto\nsynopsis: Nota $$number$$ del Taller de Edición Digital.\ncategory: \n  - Edición\n  - Publicaciones\nlanguage: es\nversion: 1.0.0\ncover: $$name$$.jpg\nnavigation: nav.xhtml\n\n# Tabla de contenidos\nno-toc: \n  - /00(1|2)/\nno-spine: \ncustom: \n\n# Si se quiere EPUB fijo\npx-width: \npx-height: \n\n# Fallbacks\nfallback: \n\n# WCAG:\nsummary: Este EPUB está optimizado para personas con deficiencias visuales; cualquier observación por favor póngase en contacto.\nmode:\n  - textual\n  - visual\nmode-sufficient:\n  - textual, visual\n  - textual\nfeature:\n  - structuralNavigation\n  - alternativeText\n  - resizeText\nhazard:\n  - none\ncontrol:\napi: ARIA"

# Va a la carpeta de notas
Dir.chdir(File.dirname(__FILE__) + '/../src/notes')

# Manda a crear cada publicación
Dir.glob('md/*.md').each do |md|
    build File.absolute_path(md)
end

# Ordena los metadatos
$notes = $notes.sort_by { |hsh| hsh[:number] }

# Va a la carpeta raíz
Dir.chdir('../..')

puts "===> Modificando main.html…"

main = []
write = true
added = false

# Modifica los enlaces a las notas del main.html
file = File.open('main.html', 'r:UTF-8')
file.each do |linea|
    if linea =~ /<!-- notes -->/
        write = !write
    end

    if write
        main.push(linea)
    else
        if !added
            main.push('                <!-- notes -->')
            main.push('                <ol>')
            $notes.each do |note|
                title = note[:title]
                        .gsub('WYSIWYG', '<span class="versalita">WYSIWYG</span>')
                        .gsub('WYSIWYM', '<span class="versalita">WYSIWYM</span>')

                main.push('                    <li>')
                main.push("                        <p>#{title}</p>")
                main.push("                        <p class=\"sin-sangria\"><a href=\"src/notes/html/#{note[:name]}.html\">HTML</a><a href=\"src/notes/epub/#{note[:name]}.epub\">EPUB</a><a href=\"src/notes/mobi/#{note[:name]}.mobi\">MOBI</a><a href=\"src/notes/md/#{note[:name]}.md\">MD</a></p>")
                main.push('                    </li>')
            end
            main.push('                </ol>')

            added = !added
        end
    end
end
file.close
file = File.new('main.html', 'w:UTF-8')
file.puts main
file.close
