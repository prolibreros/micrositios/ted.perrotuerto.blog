<section epub:type="introduction" role="doc-introduction">

# Introducción

¡Bienvenido! Si estás leyendo este manual de algún modo ya sabes de
la potencia de usar la interfaz de línea de comandos en lugar de 
interfaces gráficas.

Pero con probabilidad ese entusiasmo muta en frustración al abrir
la terminal. Sí, sabes que ahí se ponen los comandos y hasta has
ejecutado algunos… usando copiar y pegar…

En este manual se verá una introducción breve a diez comandos muy 
comunes en la terminal:

1. `ls`. Enlista los ficheros que contiene un directorio.
2. `pwd`. Imprime la ubicación actual.
3. `mkdir`. Crea un nuevo directorio.
4. `cd`. Cambia de directorio.
5. `nano`. Crea o edita archivos.
6. `cat`. Muestra el contenido de archivos y más.
7. `mv`. Mueve o renombra ficheros.
8. `cp`. Copia ficheros.
9. `rm`. Elimina ficheros.
10. `clear`. Limpia la terminal.

![La terminal en una iteración infinita clamando por tu ayuda.](../img/img01.png){.anchura-auto}

</section>
<section epub:type="frontmatter" role="doc-frontmatter">

# Qué es y qué no es este manual

En este manual no encontrarás:

* Explicaciones técnicas o teoría. Hay mucha documentación y muy
  buenos artículos sobre esto. Solo usa al [pato](https://duckduckgo.com).
* Todos los usos posibles de los comandos. Este manual es para un
  uso básico de la terminal. Ten presente que desde [el pato](https://duckduckgo.com)
  o con `man` o con el parámetro `-h` (o su sintaxis larga: `--help`) 
  puedes leer todo lo que es posible con cada comando.@note
* Un [shell](https://es.wikipedia.org/wiki/Shell_de_Unix) distinto a
  Bash. Existen muchos intérpretes de comandos, pero en este manual se
  usará [Bash](https://es.wikipedia.org/wiki/Bash) por ser el más popular.
* Instalación de Bash. En las distribuciones más populares de Linux ya
  se tiene preinstalado. Lo mismo ocurre con macOS. Si eres usuario
  de Windows, tendrás que instalar [Cygwin](https://cygwin.org)@note o
  [_Windows Subsystem for Linux_](https://docs.microsoft.com/en-us/windows/wsl/install-win10)
  antes de empezar.

En este manual sí encontrarás:

* Explicaciones sencillas y prácticas. Este manual está pensado para 
  que pronto desde la terminal te sientas como en casa.
* Los usos más comunes de los comandos. ¿Para qué complicarse desde
  un principio? Lo que verás aquí son las operaciones más habituales
  en la terminal y las nociones básicas para su uso.
* Ejercicios que te tomarán un par de minutos. Sí, sí, sí, nadie tiene
  tiempo para estar leyendo manuales extensos.
* Una analogía a las operaciones que ya haces. Aunque parezca aburrido,
  parte de la comodidad reside en caer en cuenta que lo que ya haces con
  el ratón y el entorno de escritorio lo puedes hacer desde la terminal
  y con el teclado.

</section>
<section epub:type="chapter" role="doc-chapter">

# 1. Érase una vez en tu hogar: `ls`

Como es común en cualquier sistema operativo, la manera en como interactuamos
es a través de ventanas. Con esta aproximación y usando un
explorador de archivos podemos ver las carpetas —también llamadas 
«directorios»— y archivos que de manera indistinta ambos se conocen 
como «ficheros». Cuando abrimos este explorador por lo general nos sitúa 
en nuestro _home_.

La carpeta _home_ es una de las más populares e importantes entre 
nuestros ficheros. Ahí ponemos todos los archivos que creamos o 
descargamos. Su importancia es tal que en efecto es tu hogar: ahí 
guardamos lo más íntimo y oscuro, así como su orden es proporcional al 
orden de tu vida diaria…

![Ventana que muestra el directorio _home_ y su contenido.](../img/img02.png){.anchura-auto}

La terminal no es tan distinta. Una de las principales diferencias
es que interactuamos a través de comandos en lugar de ventanas. Cuando
abrimos la terminal por lo regular nos colocará en nuestro _home_.@note

![Terminal recién abierta que nos sitúa en el _home_.](../img/img03.png){.anchura-auto}

Lo que a primera vista vemos es un texto ya impreso: `nika-zhenya en ~ $`.
Este se conoce como _prompt_ y entre muchas cosas nos indica que la
terminal ¡está a la espera de tus órdenes!

Pero antes de lanzar comandos, veamos en qué consiste comúnmente el _prompt_.
El _prompt_ por lo general y según el estilo de la terminal contiene
tres elementos:

1. Nombre del usuario. En el caso de la imagen es `nika-zhenya`.
2. Ubicación actual. Aquí sería `\~`, una manera abreviada de indicar
   la carpeta del usuario `nika-zhenya`. Es decir, `~` es igual a 
   `/home/nika-zhenya`: el hogar de Nika Zhenya.
3. Fin del _prompt_. El símbolo `$` indica que lo que viene a continuación
   son tus comandos. Aunque `$` es lo más común para señalar el final, 
   puede haber otros símbolos en su lugar como `#`.

Con estos elementos muy fácilmente podemos traducir `nika-zhenya en ~ $`
por «Este es el usuario Nika Zhenya y estás en su hogar, ¿qué deseas
hacer?». ¡Queremos ver lo que hay en la casa! Como no interactuamos
con ventanas, sino con comandos, escribimos:

```
ls
```

![Uso de `ls` para mostrar el contenido de _home_.](../img/img04.png){.anchura-auto}

Vaya, vaya, vaya, ahora podemos ver los ficheros que están adentro de
nuestro hogar, tal cual como los podemos ver cuando abrimos el explorador
de archivos. Pero no nos quedemos ahí. Así como en nuestro explorador
podemos ver los archivos ocultos, también podemos hacerlo desde la
terminal.

![Vista de archivos ocultos del _home_ desde el explorador de archivos.](../img/img05.png){.anchura-auto}

Para poder ver los archivos ocultos desde la terminal, ejecutamos:

```
ls -a
```

![Vista de archivos ocultos del _home_ desde la terminal con `ls -a`.](../img/img06.png){.anchura-auto}

Los archivos ocultos por lo general son ficheros de configuración de 
diversos programas que utilizas. Como ves, un fichero oculto siempre 
empieza con un punto, por ejemplo `.fonts` o `.icons`. Pero en no pocas 
ocasiones también demuestra qué tanto mantenimiento le das a tu
hogar… Así que mejor los volvemos a esconder debajo de la alfombra.

En nuestro explorador de archivos también tenemos una opción de ver
los ficheros a detalle. Así podemos ver cantidad de elementos,
fechas de modificación y más.

![Vista a detalle de los ficheros adentro del _home_ desde el explorador de archivos.](../img/img07.png){.anchura-auto}

Como puedes imaginarlo, esto también lo podemos hacer desde la terminal
si le ordenas:

```
ls -l
```

![Vista a detalle de los ficheros adentro del _home_ desde la terminal con `ls -l`.](../img/img08.png){.anchura-auto}

Mucha de esta información para el uso básico tiene poco sentido.
Nos quedaremos ahí, recuerda que puedes usar [el pato](https://duckduckgo.com)
o `man` o `-h` (o `--help`) para más información.

¿Quieres ver los archivos ocultos y una vista a detalle? No hay problema, 
solo ejecuta:

```
ls -a -l
```

O en su versión abreviada:

```
ls -al
```

O hasta al revés:

```
ls -la
```

Con cualquier de las formas obtendrás el mismo resultado.

![Vista a detalle y con ficheros ocultos del _home_ desde la terminal con `ls -la`.](../img/img09.png){.anchura-auto}

Como la interacción a través de comandos puede volverse compleja, 
existe un consenso en que la sintaxis se pueda escribir de la manera 
más compacta y sencilla posible. Por eso es que muchas de las opciones 
de los comandos —también conocidos como «parámetros» o «_flags_»— 
pueden unirse o escribirse _casi_ en cualquier orden.

¿Qué significa `ls`? Se trata de una abreviatura de _list_.
Al ejecutar `ls` lo que le ordenamos a la terminal es que nos muestre
una lista de los ficheros que contiene el directorio en donde nos
encontramos. Sin embargo, queremos más que eso…

</section>
<section epub:type="chapter" role="doc-chapter">

# 2. Pero ¿qué pasó ayer?: `pwd`

En más de una ocasión al llegar a nuestra casa después de una buena 
parranda nos olvidamos de dónde están las cosas. ¡Hip!, ¿dónde está
el baño? Oh, no, mañana alguien se llevará una sorpresa…

Lo mismo pasa cuando estamos en nuestro hogar digital. En ocasiones
simplemente ignoramos dónde estamos.

En el explorador de archivos por lo general siempre tenemos a la vista
nuestra ubicación en la barra superior, abajo del menú.

![Posicionamiento de la vista en la barra superior del explorador de archivos.](../img/img02.png){.anchura-auto}

En la terminal, según su configuración del _prompt_ u otras cuestiones,
puede darse el caso que no tengas una manera sencilla de saber dónde
te encuentras.

Para tener la información a la mano, basta con ejecutar:

```
pwd
```

![Impresión de la ubicación actual desde la terminal con `pwd`.](../img/img10.png){.anchura-auto}

No importa qué es lo que pase, con `pwd` siempre sabrás dónde terminaste.
Nada de resacas ni crudas morales.

¿Qué significa `pwd`? Es una abreviatura de _print working directory_.
Con este comando ordenamos a la terminal que nos indique cuál es el
directorio donde estamos trabajando.

</section>
<section epub:type="chapter" role="doc-chapter">

# 3. Un poco de diversión: `mkdir`

Ver lo que está adentro del _home_ y siempre tener la brújula del `pwd`
es muy útil. Pero también muy aburrido. Una manera de divertirnos un poco
es empezar a crear más directorios.

Con el explorador de archivos la creación es muy sencilla: con clic
derecho podemos crear una nueva carpeta.

Desde la terminal también podemos hacer lo mismo si ejecutamos:

```
mkdir borrame
```

![Creación de un directorio desde la terminal con `mkdir`.](../img/img11.png){.anchura-auto}

Con este comando estaremos creando una nueva carpeta con el nombre
«borrame». Puedes comprobarlo si abres tu explorador de archivos.

![Vista de la carpeta creada (resaltada con verde) desde el explorador de archivos.](../img/img12.png){.anchura-auto}

Pero como ya viste, también puedes corroborarlo si ejecutas `ls`.

![Vista de la carpeta creada (resaltada con verde) desde la terminal.](../img/img13.png){.anchura-auto}

¿Deseas crear una carpeta adentro de un directorio? No hay problema,
escribe:

```
mkdir borrame/nivel1-a
```

¿Quieres crear múltiples carpetas adentro de un directorio? Puedes
señalar diferentes directorios si los separas con un espacio:

```
mkdir borrame/nivel1-b borrame/nivel1-c
```

Hasta puedes crear una subcarpeta adentro de una carpeta que a su vez
está adentro de otro directorio:

```
mkdir borrame/nivel1-a/nivel2-a
```

![Creación de carpetas y subcarpetas en un mismo directorio desde la terminal.](../img/img14.png){.anchura-auto}

Toma un tiempo en analizar todos los comandos que acabas de ejecutar.
Puede ser muy confuso al principio. Por fortuna si ves el explorador
de archivos, puedes observar la estructura que acabas de crear adentro
de `borrame`.

![Vista de los contenidos de `borrame` desde el explorador de archivos.](../img/img15.png){.anchura-auto}

Puedes hacer algo análogo en la terminal si, por ejemplo, ejecutas 
`ls borrame` o `ls borrame/nivel1-a`.@note

![Vista de los contenidos de `borrame` desde la terminal con `ls`.](../img/img16.png){.anchura-auto}

De esta manera podemos crear directorios sin ningún problema. Pero hay
dos detalles que se tienen que mencionar para empezar a comprender un
aspecto muy importante de la terminal: el uso de rutas, también conocidas
como _paths_.

La estructura que creamos con `mkdir` es la siguiente:

```
borrame
├── nivel1-a
│   └── nivel2-a
├── nivel1-b
└── nivel1-c
```

Con esto tenemos que `borrame` es el nivel 0 —es un número relativo,
no lo tomes tan en serio—. Todo lo que este fichero contiene de manera 
inmediata está en el nivel 1, como `nivel1-a`, `nivel1-b` y `nivel1-c`. 
Por último, tenemos una subcarpeta `nivel2-a` en el nivel 2, ya que 
está adentro de la carpeta `nivel1-a`.

La manera visual en como el explorador de archivos muestra este
anidamiento es mediante el uso de flechas y espacios, conocidos como 
acordeones.

![Vista a detalle del acordeón que muestra el explorador de archivos.](../img/img17.png){.anchura-auto}

Para poder expresar esta ruta desde la terminal necesitamos de un
delimitador por el cual se pueda distinguir un nivel de otro. Para esto
se usa la barra (`/`). Por ejemplo, esta ruta:

```
borrame
└── nivel1-a
    └── nivel2-a
```

Es lo mismo a decir: «De la carpeta `borrame` _nos movemos_ a la carpeta
`nivel1-a` para de nuevo _movernos_ a la carpeta `nivel2-a`». Es decir,
estamos expresando: «Nivel 0 > Nivel 1 > Nivel 2» que en este ejemplo
es `borrame > nivel1-a > nivel2-a`. Para escribir esta ruta con una
sintaxis correcta basta con sustituir `>` por `/`: `borrame/nivel1-a/nivel2-a`.

Para comprobar que esta ruta existe puedes ejecutar `ls borrame/nivel1-a/nivel2-a`…
¡No te mostrará nada, porque el fichero `nivel2-a` está vacío!

En cambio, si ejecutas:

```
ls borrame/nivel1-a/nivel2-b
```

![Intento de enlistar los contenidos de una carpeta inexistente desde la terminal.](../img/img18.png){.anchura-auto}

Nos muestra un error porque en efecto no tenemos una subcarpeta `nivel2-b`
adentro de la carpeta `nivel1-a` del fichero `borrame`. El uso de `ls`
puede ser de gran ayuda para indagar cuando una ruta es inexistente o
simplemente está mal escrita.

El segundo gran detalle es el nombre de los archivos. Como puedes observar,
las carpetas que acabamos de crear tienen las siguientes características
en su sintaxis:

* Están escritas en minúsculas.
* No contienen espacios y en su lugar se utilizan guiones.
* No contienen tildes.

Esto _no se debe_ a que la terminal es incapaz de reconocer mayúsculas,
espacios o tildes. Por comodidad es más sencillo emplear este consenso 
para escribir las rutas. No solo es más claro para ti, también puedes
evitar conflictos con las rutas cuando uses otros programas desde la 
terminal.

Solo son ficheros, ¿vale? Aunque la redacción correcta y más agradable
sea «Bórrame» o «Nivel 1 a», por comodidad es mejor dejarlas como
`borrame` o `nivel1-a` o `nivel_1_a`. Como quieras, ¡ahora puedes crear
carpetas con el nombre que te agrade!

¿Qué significa `mkdir`? Es una abreviación de _make directory_. Con
este comando le decimos a la terminal que haga directorios.

</section>
<section epub:type="chapter" role="doc-chapter">

# 4. Clases de cumbia: `cd`

Ya no queremos estar en el _home_, ¡hay que empezar a bailar!

Con el explorador de archivos el movimiento es muy sencillo: por medio
de clics o presionando `Enter` sobre los directorios cambiamos de un 
lugar a otro. Lo que en realidad hacemos es indicar con el puntero la 
serie de pasos a seguir para llegar a una ubicación específica.

Con la terminal es prácticamente lo mismo. Por ejemplo, nuestro primer
paso es ir hacia el fichero `borrame`. ¿Listo? Uno, ejecuta:

```
cd borrame
```

![Primer paso de la cumbia linuxera.](../img/img19.png){.anchura-auto}

Dos:

```
cd nivel1-a
```

![Segundo paso de la cumbia linuxera.](../img/img20.png){.anchura-auto}

Tres:

```
cd ..
```

![Tercer paso de la cumbia linuxera.](../img/img21.png){.anchura-auto}

Cuatro:

```
cd ..
```

![Cuarto paso de la cumbia linuxera.](../img/img22.png){.anchura-auto}

Analicemos un poco los pasos. En el primero nos fuimos hacia adelante:

```
home
──> borrame
```

Esto lo puedes comprobar con el _prompt_ ya que una vez hecho el
paso puedes ver que tu ubicación es `\~/borrame` y ya no `~`. 

El segundo paso también fue hacia adelante:

```
home
──> borrame
    ──> nivel1-a
```

De nuevo puedes corroborarlo con el _prompt_ o incluso puedes utilizar
otros comandos que ya usamos, como `ls` o `pwd`. 

Con el tercer paso retrocedimos un poco:

```
home
──> borrame
    <── nivel1-a
```

Así como el cuarto paso sirvió para que regresáramos a nuestro _home_:

```
home
<── borrame
```

Aquí hay algo interesante para resaltar. Como ya vimos, para indicar
rutas en la terminal usamos la sintaxis `nivel_0/nivel_1/nivel_2`.
Esto es lo mismo a «Nivel 0 > Nivel 1 > Nivel 2».

Pero esta sintaxis nos hace ir hacia adelante… Solo nos permite ir 
cada vez más adentro de nuestros ficheros. No obstante, ¿cómo podemos
regresar? ¿Cómo es posible ir a un nivel más arriba?

Con nuestro explorador de archivos es tan sencillo como usar el botón
o la tecla de retroceso. En la terminal también es muy fácil: con dos 
puntos consecutivos (`..`) indicamos un paso hacia atrás.

Es decir, cada vez que quieras ir para atrás imagina que nuestro
botón de retroceso en la terminal es un simple `..`.

Con este conocimiento, más lo que hemos estado aprendiendo sobre las 
rutas, ¡es posible dar los pasos más rápidos! Vamos, uno, dos:

```
cd borrame/nivel1-a
```

![Primer y segundo paso de la cumbia linuxera: para adelante.](../img/img23.png){.anchura-auto}

¡Ahora tres, cuatro!:

```
cd ../..
```

![Tercer y cuarto paso de la cumbia linuxera: para atrás.](../img/img24.png){.anchura-auto}

Podemos ir incluso más adentro:@note

```
cd borrame/nivel1-a/nivel2-a
```

Y regresar:

```
cd ../../..
```

Ahora bien, suponiendo que tenemos más niveles adentro de `borrame`,
¿cómo haríamos para regresar a nuestro _home_? Bien podemos irnos
para atrás _n_ número de veces con `..`.

Sin embargo, hay veces que ya estamos tan adentro que ese tipo de
retorno se vuelve poco practico. Para estos casos siempre desde 
cualquier ubicación podemos regresar a nuestro _home_ al ejecutar:

```
cd
```

![Regreso al _home_ desde la terminal usando `cd`.](../img/img25.png){.anchura-auto}

Como puedes adivinar, si al comando `cd` no le indicas ninguna ruta,
este considerará que quieres regresar al `home`.

Así ya tenemos tres elementos muy importantes para considerar en nuestras
rutas:

1. Para indicar la diferencia entre niveles usamos `/`.
2. Para avanzar solo escribimos el nombre del siguiente fichero que
   está adentro del fichero que le antecede.
3. Para retroceder basta con `..`.

Entonces, ¿a dónde vamos si ejecutamos lo siguiente?

```
cd borrame/nivel1-a/../
```

![Más pasos de la cumbia linuxera: hacia adelante y adelante y hacia atrás.](../img/img26.png){.anchura-auto}

¡A `borrame`! ¿Cómo? Veamos los pasos:

```
home
──> borrame
    ──> nivel1-a
    <── nivel1-a
```

Con estos sencillos pasos hacia adelante y hacia atrás cuentas con un
sin fin de combinaciones que te permitirán ir a cualquier parte de tus
ficheros. ¿Quieres probarlo? Siente el ritmo de tu terminal y ponte a
bailar aquí y allá.

¿Qué significa `cd`? Se trata de una abreviación para _change directory_.
Al ejecutar `cd` lo que le pedimos a la terminal es que nos acompañe
a bailar al son del cambio de directorios.

</section>
<section epub:type="chapter" role="doc-chapter">

# 5. Y el verbo se hizo bit: `nano`

Al fin podemos movernos desde la terminal y crear directorios. Pero
¿acaso no podemos también crear documentos? En el explorador de archivos
al hacer clic derecho podemos crear nuevos archivos en blanco.

Con la terminal podemos hacer lo mismo con `nano`.@note Por ejemplo, adentro
de `borrame` ejecutemos:

```
nano mi_primer_archivo
```

![Entorno al ejecutar `nano` en la terminal.](../img/img27.png){.anchura-auto}

¡De manera inmediata `nano` nos muestra su entorno para poder
empezar a redactar! Qué tal si escribimos:

```
Hola, mundo.
```

Para salir presionamos `Ctrl + X`.@note Nos preguntará si queremos guardar
los cambios, a lo que responderemos que sí con `Y` o `S` y pulsamos
`Enter`.

Desde el explorador de archivos puedes comprobar que ¡el archivo existe!

![El fichero `mi_primer_archivo` visto desde el explorador de archivos.](../img/img28.png){.anchura-auto}

Y como bien sabes, también puedes verlo si ejecutas `ls`.

![El fichero `mi_primer_archivo` visto desde la terminal con `ls`.](../img/img29.png){.anchura-auto}

Al intentar abrir el archivo haciendo clic desde el explorador de archivos
te puede ocurrir algo de lo siguiente:

1. Te lo abrirá en tu editor de texto por defecto como puede ser 
   Gedit, TextEdit o Bloc de notas.
2. Te preguntará con qué programa quieres abrir el archivo.
3. Te indicará que el fichero no pudo ser abierto.

Esto sucede porque nos acercamos a una distinción rara vez percibida
por la mayoría de los usuarios. En un entorno gráfico y en el uso de
_software_ propietario tendemos a confundir el tipo de archivo con
el programa. 

Un ejemplo clásico es el archivo +++DOCX+++ de Microsoft. ¿Cuántas veces 
en lugar de llamarlo +++DOCX+++ dijiste «Archivo de Word»? En efecto 
Microsoft creó el formato +++DOCX+++ para abrirlo con Microsoft Word. 
Sin embargo, una cuestión es el tipo de archivo —el +++DOCX+++— y el 
programa con el que abrimos el archivo —Microsoft Word—.

Como usuarios tendemos a asociar de manera unívoca un tipo de archivo
con un programa. Incluso llegamos al punto de pensar que para cierto
tipo de documento necesitamos algún programa específico.

Regresando al ejemplo, podemos darnos cuentas que son cosas distintas
cuando:

1. Abrimos el +++DOCX+++ con otro programa, como [LibreOffice Writer](https://es.libreoffice.org/).@note
2. Usamos Microsoft Word para abrir otros formatos como +++DOC+++, 
   +++RTF+++, +++TXT+++ o +++HTM+++.

Para evitar que el explorador de archivos intente descifrar cuál es
el programa más conveniente para abrir el documento, lo mejor es guardarlo
ya con una extensión definida. La extensión son esas letras que vienen
después de un punto en los archivos como `.txt`, `.md` o `.docx`. Con
esto indicamos que ese fichero es un archivo de texto plano, un documento
de Markdown o un «Archivo de Word», respectivamente.

Entonces, hagamos nuestro segundo documento con `nano`:

```
nano mi_segundo_archivo.txt
```

Escribe «Hola de nuevo, mundo.», cerramos y guardamos: `Ctrl + X`, `Y` 
y `Enter`. Ahora si lo intentas abrir desde tu explorador de archivos,
no tendría que haber ningún problema.

![El fichero `mi_segundo_archivo.txt` visto desde LibreOffice Writer.](../img/img30.png){.anchura-auto}

Como ya te has dado cuenta, puedes editar tus archivos creados con 
`nano` desde cualquier editor de tu conveniencia: no necesitas hacerlo 
todo desde la terminal. También puedes editar desde `nano` archivos 
creados con otros programas.

¿Puedo crear o editar cualquier tipo de documento con `nano`? Por
desgracia no. Desde la terminal solo podemos crear correctamente y
editar [archivos de texto plano](https://es.wikipedia.org/wiki/Archivo_de_texto).
Se trata de ficheros que no tienen ningún formato tipográfico, aunque
sí es posible trabajar con archivos de lenguaje de marcado como Markdown,
+++HTML+++, +++XML+++ o TeX.

![Intento fallido de editar con `nano` un archivo +++DOCX+++ que solo contiene «Hola, mundo».](../img/img31.png){.anchura-auto}

Aunque por comodidad preferimos trabajar con editores de texto con 
entornos gráficos, `nano` es una buena opción para crear archivos de 
manera instantánea o para hacer ediciones rápidas.

¿Qué significa `nano`? Es un [acrónimo recursivo](https://es.wikipedia.org/wiki/Acr%C3%B3nimo_recursivo)
que indica: «Nano's ANOther editor». Sí, a los programadores les
encantan las bromas «recursivas». Aunque no sean tan graciosas…

</section>
<section epub:type="chapter" role="doc-chapter">

# 6. La curiosidad ¿mató al gato?: `cat`

El comando `nano` puede ser muy versátil. Pero hay ocasiones que por
urgencia o comodidad solo queremos ver el contenido de los archivos.

En el explorador de archivos de algunos sistemas operativos podemos
ver rápidamente su contenido. Muchas veces no es cómodo y solo nos
muestra los primeros caracteres.

En la terminal tenemos un comando que nos permite curiosear: `cat`.
Mejor que explicarlo, por qué no ejecutas:

```
cat mi_primer_archivo && cat mi_segundo_archivo.txt 
```

![Los contenidos de los ficheros `mi_primer_archivo` y `mi_segundo_archivo.txt` vistos desde la terminal con `cat`.](../img/img32.png){.anchura-auto}

La terminal nos ha arrojado los contenidos de ambos archivos según el
orden indicado. También puedes ver únicamente el contenido de un archivo,
solo simplifica el comando anterior:

```
cat mi_primer_archivo
```

![El contenido del fichero `mi_primer_archivo` visto desde la terminal con `cat`.](../img/img33.png){.anchura-auto}

Aquí tenemos algunas consideraciones relevantes. Primero, tal como
`nano`, con `cat`:

* Siempre utilizamos la misma sintaxis para indicar las rutas a los ficheros
  (para ser sinceros, esto aplica a todos los comandos).
* Solo podemos ver el contenido de archivos de texto plano o de lenguajes
  de marcado o todo aquel que sea un [formato abierto](https://es.wikipedia.org/wiki/Formato_abierto).

En otro aspecto, como puedes ver en el primer ejemplo de `cat`, es
posible ejecutar varios comandos en una sola línea con `&&`. Esta sintaxis
indica una conjunción: `cat mi_primer_archivo` + `cat mi_segundo_archivo.txt`.
Puede ser cualquier comando, por ejemplo:

```
ls -l && cat mi_primer_archivo
```

![Lista del fichero `borrame` y vista del contenido de `mi_primer_archivo` desde la terminal con `ls` y `cat`.](../img/img34.png){.anchura-auto}

En realidad el comando `cat mi_primer_archivo && cat mi_segundo_archivo.txt`
fue una excusa para mostrar cómo hacer conjunciones con Bash. Si solo
usas `cat` para varios archivos, puedes ejecutar de manera más simplificada:

```
cat mi_primer_archivo mi_segundo_archivo.txt 
```

![El contenido del fichero `mi_primer_archivo` visto de manera más sencilla desde la terminal con `cat`.](../img/img35.png){.anchura-auto}

Basta con separar cada ruta con al menos un espacio para indicarle a
`cat` que estamos trabajando con varios archivos al mismo tiempo.

Pero esto no es todo, ¿qué tal si no solo quieres ver varios archivos,
sino crear uno nuevo que contenga todo ese contenido? Podemos valernos
de otro operador de Bash: `>`.@note Con este ordenamos que todo lo que se
encuentre al lado izquierdo se vierta en el fichero del lado derecho:

```
cat mi_primer_archivo mi_segundo_archivo.txt > mi_tercer_archivo.txt
```

Al parecer no ha sucedido nada… ¿y si enlistamos los ficheros y hacemos
cat a `mi_tercer_archivo.txt`?

```
ls -l && cat mi_tercer_archivo.txt
```

![Lista del fichero `borrame` y vista del contenido de `mi_tercer_archivo.txt` desde la terminal con `ls` y `cat`.](../img/img36.png){.anchura-auto}

Si aún no lo crees, puedes abrir el archivo en cualquier editor de
texto…

![Vista del contenido de `mi_tercer_archivo.txt` desde un editor de texto.](../img/img37.png){.anchura-auto}

El comando `cat` es muy poderoso porque nos permite realizar muestreos
rápidos y unir contenidos. Pero por lo mismo puede ser peligroso. Si al
momento de concatenar archivos, el fichero final (el que está al lado
derecho de `>`) tiene el mismo nombre que uno de los documentos de
entrada (los que estén al lado izquierdo de `>`), provocarás que ese
archivo tenga un nuevo contenido…

El comando `cat` no pregunta por permiso y es muy curioso. Recuerda
que a los gatos no puedes domarlos.

¿Qué significa `cat`? No es honor a los felinos, sino de _concatenate_.
Con `cat` podemos ver pero principalmente concatenar archivos.

</section>
<section epub:type="chapter" role="doc-chapter">

# 7. Ya lo sé que tú te vas: `mv`

Ya sabemos cómo crear ficheros, editarlos, mostrarlos y unirlos. Pero 
tenemos poco o nada de control sobre nuestros archivos si no podemos 
cambiarlos de lugar.

Para mover archivos solo tenemos que indicar cuáles son los ficheros
de origen y al final cuál es el directorio destino. Por ejemplo, ejecuta:

```
mv mi_primer_archivo nivel1-a/nivel2-a/
```

Como es común, la terminal no te comentará nada, pero bien puedes enlistar
la carpeta `nivel1-a/nivel2-a/` para corroborarlo (o ver el explorador de archivos):

```
ls nivel1-a/nivel2-a/
```

![Traslado de `mi_primer_archivo` a `nivel1-a/nivel2-a/` desde la terminal con `mv`.](../img/img38.png){.anchura-auto}

Tal como vimos con `cat` podemos trabajar con varios ficheros al mismo 
tiempo. ¿Quieres mover `mi_segundo_archivo.txt` y `mi_tercer_archivo.txt` 
a `nivel1-c`?, ejecuta:

```
mv mi_segundo_archivo.txt mi_tercer_archivo.txt nivel1-c && ls nivel1-c
```

![Traslado y vista de `mi_segundo_archivo.txt` y `mi_tercer_archivo.txt` a `nivel1-c` desde la terminal con `mv` y `ls`.](../img/img39.png){.anchura-auto}

Hasta aquí, tu árbol de directorios debería de lucir así:

```
.
├── nivel1-a
│   └── nivel2-a
│       └── mi_primer_archivo
├── nivel1-b
└── nivel1-c
    ├── mi_segundo_archivo.txt
    └── mi_tercer_archivo.txt
```

¿Qué es ese punto? Así como hemos visto que los dos puntos consecutivos
(`..`) indican un nivel más arriba en la jerarquía, el punto (`.`) señala
el directorio actual. En este caso `.` es igual a `borrame`.

Como en la terminal siempre se busca escribir lo menos posible, ese
punto puede ser muy útil al momento de trabajar. Por ejemplo, 
regresemos a `mi_primer_archivo` a nuestro fichero `borrame`:

```
mv nivel1-a/nivel2-a/mi_primer_archivo . && ls -l
```

![Traslado y vista de `mi_primer_archivo` a `borrame` desde la terminal con `mv` y `ls`.](../img/img40.png){.anchura-auto}

El punto es relativo, por lo que si nos movemos a `nivel1-b` con `cd`:

```
cd nivel1-b
```

Ahora el directorio `nivel1-b` es `.`. 

Con lo que hemos visto de las rutas, más el significado de los dos puntos 
consecutivos o del punto contamos, ya con todos los elementos necesarios 
para movernos y más.

Podemos mover todos los archivos `.txt` al directorio `nivel1-b` si 
ejecutamos:

```
mv ../nivel1-c/*.txt . && ls
```

![Traslado y vista de los archivos `.txt` a `nivel1-b` desde la terminal con `mv` y `ls`.](../img/img41.png){.anchura-auto}

Momento, ¿qué quiere decir `*.txt`? No hay ningún archivo que se llame 
así. En la carpeta `nivel1-c` habíamos enviado a 
`mi_segundo_archivo.txt` y `mi_tercer_archivo.txt`, pero ¡ninguno se 
llama `*.txt`!

El asterisco (`*`) es un símbolo que en Bash se emplea como un comodín
y _puede ser usado en cualquier comando_ no solo en `mv`. Con `*.txt` 
se expresó «cualquier cosa con terminación `.txt`». Los archivos 
`mi_segundo_archivo.txt` y `mi_tercer_archivo.txt` cumplen con esa 
condición.

Este comodín puede ir en cualquier lugar. Por ejemplo, ejecuta:

```
mv mi_* .. && cd .. && ls
```

![Traslado y vista de los archivos `mi_*` a `borrame` desde la terminal con `mv`, `cd` y `ls`.](../img/img42.png){.anchura-auto}

Aunque la sintaxis del pasado comando ya la hemos trabajado, quizá valga
la pena ver qué fue lo que hicimos:

1. En una sola línea ejecutamos tres comandos (`mv`, `cd` y `ls`) los
   cuales fueron separados por `&&`.
2. Con `mv mi_* ..` ordenamos mover todos los archivos que inician con
   `mi_` a un nivel más arriba. Como estábamos en `nivel1-b`, quiere
   decir que `borrame` fue el destino.
3. Con `cd ..` solo nos regresamos a `borrame`.
4. Para terminar, `ls` se ha estado usando para mostrar el contenido
   de `borrame`. No era necesario ejecutarlo, solo fue ilustrativo.

Con todos los movimientos, ahora tendrías que tener esta estructura:

```
.
├── mi_primer_archivo
├── mi_segundo_archivo.txt
├── mi_tercer_archivo.txt
├── nivel1-a
│   └── nivel2-a
├── nivel1-b
└── nivel1-c
```

Con `mv` también podemos mover carpetas. Qué tal si creamos primero una
llamada `nivel2-c`:

```
mkdir nivel2-c
```

Ahora metamos todos los archivos en ese directorio con un comodín. No
podemos hacerlo con `*.txt` porque ¡`mi_primer_archivo` no tiene extensión!
Pero todos empiezan con `mi_`, así que podemos ejecutar:

```
mv mi_* nivel2-c && ls
```

![Traslado y vista de los archivos `mi_*` a `nivel2-c` desde la terminal con `mv` y `ls`.](../img/img43.png){.anchura-auto}

¡Ups! Revisando la estructura, la hemos regado un poco:

```
.
├── nivel1-a
│   └── nivel2-a
├── nivel1-b
├── nivel1-c
└── nivel2-c
    ├── mi_primer_archivo
    ├── mi_segundo_archivo.txt
    └── mi_tercer_archivo.txt
```

La carpeta `nivel2-c` tuvimos que haberla creado adentro del fichero 
`nivel1-c`. ¡No hay problema!, ejecutemos:

```
mv nivel2-c nivel1-c
```

¡Listo! ya hemos movido también una carpeta. Nuestra estructura
quedaría como la siguiente:

```
.
├── nivel1-a
│   └── nivel2-a
├── nivel1-b
└── nivel1-c
    └── nivel2-c
        ├── mi_primer_archivo
        ├── mi_segundo_archivo.txt
        └── mi_tercer_archivo.txt
```

¿Qué significa `mv`? Aunque bien las siglas corresponden a «mover», es
una abreviatura para _move_. Sí, los comandos por lo regular son _shortcuts_
de palabras en inglés…

</section>
<section epub:type="chapter" role="doc-chapter">

# 8. Copiar y pegar en un solo comando: `cp`

Para los defensores del `copyright` no hay peor mal que el _copy & paste_.
Por fortuna en los mundos de +++GNU+++, Linux y Bash todos aman las copias.

En el explorador de archivos tenemos la función de copiar y pegar separadas.
Sin embargo, desde la terminal esta tarea se realiza en una sola línea.
Su sintaxis es muy similar a `mv`: los ficheros a copiar, conocidos como
origen, se colocan antes del fichero destino, el cual es el último elemento.

Por ejemplo, empecemos por copiar a `mi_primer_archivo` en `borrame`:

```
cp nivel1-c/nivel2-c/mi_primer_archivo . && ls
```

![Copia y vista del archivo `mi_primer_archivo` a `borrame` desde la terminal con `cp` y `ls`.](../img/img44.png){.anchura-auto}

Como puedes observar, el archivo se copió con el mismo nombre. Hay ocasiones
que deseamos copiarlo con uno distinto. Para esto solo hay que especificarlo, 
ejecutando:

```
cp mi_primer_archivo mi_primer_archivo-2 && ls
```

![Copia y vista del archivo `mi_primer_archivo` como `mi_primer_archivo-2` desde la terminal con `cp` y `ls`.](../img/img45.png){.anchura-auto}

Semejante a las uniones de archivos con `cat`, `cp` no pregunta si deseas 
sobreescribir el fichero. Por ello tienes que tener cuidado en no asignar 
el mismo nombre de un archivo a la copia. ¡No vayas a perder datos!

Es posible copiar varios archivos en un solo comando. La vía más larga
es escribir cada ruta separada por un espacio. Como bien lo estás pensando,
también podemos usar el comodín (`*`):

```
cp nivel1-c/nivel2-c/mi_* . && ls
```

![Copia y vista de los archivos `mi_*` a `borrame` desde la terminal con `cp` y `ls`.](../img/img46.png){.anchura-auto}

Cabe destacar que cuando se copian varios archivos no es posible asignarles
un nombre distinto, solo es posible indicar el directorio destino. Hasta
ahora, la estructura que deberías de tener es:

```
.
├── mi_primer_archivo
├── mi_primer_archivo-2
├── mi_segundo_archivo.txt
├── mi_tercer_archivo.txt
├── nivel1-a
│   └── nivel2-a
├── nivel1-b
└── nivel1-c
    └── nivel2-c
        ├── mi_primer_archivo
        ├── mi_segundo_archivo.txt
        └── mi_tercer_archivo.txt
```

Pero hablando de directorios, ¿probamos en copiar uno?

```
cp nivel1-b nivel1-b_2
```

![Intento fallido de copiar el directorio `nivel1-b` desde la terminal con `cp`.](../img/img47.png){.anchura-auto}

Mmm…, ¿qué ha pasado? El error nos indica que el parámetro `-r` no
fue especificado. Esto quiere decir que no pedimos que la copia fuera
recursiva.

El concepto de «[recursión](https://es.wikipedia.org/wiki/Recursi%C3%B3n)»
puede ser muy complejo. En estos casos quiere decir que el comando `cp`
no solo tiene que aplicarse al fichero de origen, sino también a todos 
los posibles elementos que contiene.

Para el caso de los archivos la recursión no es necesaria porque por
definición no contienen otros ficheros adentro de ellos. Sin embargo,
los directorios son ficheros que sí pueden contener más ficheros. 

La analogía con las carpetas ayuda a darse cuenta que, si quieres copiarla, 
también tendrías que copiar todos sus elementos, ¿no es así? Es decir,
necesitas indicar que el comando `cp` también se repita en cada uno de
los posibles ficheros que están adentro de esa carpeta:

```
cp -r nivel1-b nivel1-b_2 && ls
```

![Copia y vista del directorio `nivel1-b` como `nivel1-b_2` desde la terminal con `cp` y `ls`.](../img/img48.png){.anchura-auto}

Con `cp` podemos ¡copiar y pegar al infinito y más allá!

¿Qué significa `cp`? Se trata de una manera corta para _copy_. Con este
comando podemos copiar y pegar cualquier fichero desde la terminal.

</section>
<section epub:type="chapter" role="doc-chapter">

# 9. _Mr. Stark, I don't feel so good_: `rm`

Hasta ahora tenemos un árbol como este:

```
.
├── mi_primer_archivo
├── mi_primer_archivo-2
├── mi_segundo_archivo.txt
├── mi_tercer_archivo.txt
├── nivel1-a
│   └── nivel2-a
├── nivel1-b
├── nivel1-b_2
└── nivel1-c
    └── nivel2-c
        ├── mi_primer_archivo
        ├── mi_segundo_archivo.txt
        └── mi_tercer_archivo.txt
```

Hay muchas copias ahí. ¡Está sobrepoblado! Así que empecemos a eliminar
uno por uno de nuestros archivos:

```
rm mi_primer_archivo
```

Pero esto nos puede tomar mucho tiempo. Probemos con agilizar un poco
las cosas:

```
rm mi_primer_archivo-2 \
> mi_segundo_archivo.txt \
> mi_tercer_archivo.txt && ls
```

![Eliminación y vista de todos los archivos en `borrame` desde la terminal con `rm` y `ls`.](../img/img49.png){.anchura-auto}

Hay ocasiones donde no queremos usar el comodín (`*`) pero al mismo
tiempo queremos tener una sintaxis más clara en lugar de escribirlo
todo en una sola línea.

Bash nos permite utilizar saltos de línea para hacer nuestra redacción
más legible. Estos saltos se señalan con una diagonal inversa (o barra
invertida, como te agrade). Así que el pasado comando en realidad es
lo mismo a `rm mi_primer_archivo-2 mi_segundo_archivo.txt mi_tercer_archivo.txt && ls`.@note

Todavía con estos elementos la eliminación puede tardar mucho tiempo.
¿Qué tal si mejor eliminamos carpetas enteras? Para eso ejecutamos:

```
rm nivel1-a
```

![Intento fallido de eliminar el directorio `nivel1-a` desde la terminal con `rm`.](../img/img50.png){.anchura-auto}

Algo ha fallado de nuevo… ¿No será porque el comando `rm` requiere de 
recursión, ya que se trata de una carpeta? Veamos:

```
rm -r nivel1-a
```

![Eliminación del directorio `nivel1-a` y vista de `borrame` desde la terminal con `rm` y `ls`.](../img/img51.png){.anchura-auto}

¡Eso era! Pero aún así puede ser muy monótono eliminar carpeta por carpeta.
Podemos borrar la carpeta `borrame` de tajo si nos vamos un nivel más
arriba y le aplicamos `rm`. Aunque también con un chasquido de los
dedos (y un comodín) podemos eliminarlas todas sin deshacernos del
fichero principal:

```
rm -r *
```

![Eliminación de todos los ficheros adentro de `borrame` desde la terminal con `rm`.](../img/img52.png){.anchura-auto}

La eliminación con `rm` es muy rápida y poderosa… y por ende muy peligrosa. 
La eliminación de ficheros a través del explorador de archivos siempre 
los manda a la papelera. En el caso de `rm`, la eliminación es directa: 
_no hay manera de recuperar los ficheros por medio de la papelera_. 
¡Ten cuidado!

Al fin hemos eliminado la sobrepoblación de `borrame`, ya podemos
estar tranquilos, regresar a casa y olvidar que todo esto ha ocurrido:

```
cd .. && rm -r borrame
```

¿Qué significa `rm`? A esta altura es fácil de adivinar: _remove_. 
Con `rm` podemos ser Thanos y extinguirlo todo con un par de movimientos 
de dedos. O casi todo… en algunas ocasiones necesitaremos forzar las
cosas con `-f` o incluso tener permisos de administrador con `sudo`,
no solo con `rm` sino con diversos comandos. Pero ¡eso es otra historia!

</section>
<section epub:type="chapter" role="doc-chapter">

# 10. Limpiando el desorden: `clear`

Ya llegamos al final de este manual y la terminal te lo quiere agradecer.
Me pide de favor si te puedo decir que ejecutes lo siguiente:

```
while /bin/true; do echo "Gracias :)"; done
```

![La terminal en una iteración infinita de agradecimiento.](../img/img53.png){.anchura-auto}

¡Ups! La terminal nos ha hecho crear un [bucle infinito](https://es.wikipedia.org/wiki/Bucle_infinito).
Nada puede detenerlo y somos incapaces de usar la terminal… al menos
que usemos `^C` (`Ctrl + C`).

![Cancelación del proceso desde la terminal con `^C`.](../img/img54.png){.anchura-auto}

En varias ocasiones mientras trabajes con la terminal te encontrarás con
un proceso que no puede ser finalizado. Así como en el entorno de
escritorio hay programas que dejan de funcionar, en la terminal también
podemos experimentarlo.

Para esas indeseadas ocasiones siempre podemos recurrir a cancelar la
ejecución con `^C`. No importa qué estés haciendo, `Ctrl + C` siempre
debería de funcionar.

Esta fue una pequeña broma —ya es evidente que los chascarrillos no
son tan buenos, ¿cierto?—. Pero en internet habitan muchos troles y 
una broma puede salirse de control, como las [bombas fork](https://www.poftut.com/crash-linux-system-with-a-bash-fork-bomb/).
En la medida de lo posible copia y pega comandos que al menos tengas
una idea general de lo que realizan.

Para limpiar el texto de la terminal, en cualquier momento puedes ejecutar:

```
clear
```

![Limpieza de la terminal con `clear`.](../img/img03.png){.anchura-auto}

Este comando desaparecerá lo impreso en la terminal para únicamente
mostrarnos de nuevo el _prompt_. Úsalo cada vez que necesites un poco
de orden o tengas una manía por la limpieza.

</section>
<section epub:type="conclusion" role="doc-conclusion">

# Practica, practica, practica

Este manual solo fue una breve introducción a la terminal. Varias de las
cosas que aprenderás a partir de ahora de una u otra manera implicarán
los comandos descritos aquí.

Esto se debe a que, al final, podemos decir que las operaciones realizadas
a través de la terminal son siempre en torno a ficheros. Sea en un
ambiente de escritorio o desde la terminal, en las computadoras siempre
estamos creando, editando, moviendo, copiando o eliminando archivos.

Por esto, la última recomendación es que no dejes de practicar. No 
olvides la lógica de las rutas. Tampoco desatiendas la breve sintaxis
que vimos de Bash. Siempre recuerda estos diez comandos básicos. Todo 
esto se pagará con creces en el futuro: un pequeño paso para ti, pero 
un gran paso hacia el empoderamiento tecnológico.

Mientras tanto, ¡cambio y fuera!

</section>
