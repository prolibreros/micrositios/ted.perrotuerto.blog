<section epub:type="chapter" role="doc-chapter">

# Respaldo de la información

Hay ocasiones en que el proyecto completo se arruina: archivos 
corruptos, pérdida de documentos, malfuncionamiento de la computadora, etcétera.
¿Cuál es la mejor manera de gestionar cualquier clase de proyecto digital? Como
lo es la edición de una obra…

![Logotipo de Git.](../img/s09_img01.jpg)

## Recursos

1. «[Edición cíclica y edición ramificada: un vuelo seguro y constante (3/3)](http://marianaeguaras.com/edicion-ciclica-edicion-ramificada-vuelo-seguro-constante/)»

----

## Modo común de respaldar información

La manera usual de respaldar información es mediante **la generación
de copias** que se almacenan en:

* La misma computadora.
* Un disco duro externo.
* La «nube».

Sin embargo, esto acarrea los siguientes inconvenientes:

* Mayor necesidad de espacio cada vez que se crea una copia.
* Posibilidad de confusión entre los respaldos y el proyecto actual.
* Falta de control en los cambios, principalmente en grandes equipos.
* Dificultades al trabajar de manera separada o remota.
* Desconocimiento de casi todas las modificaciones que se han llevado a cabo.

**El descuido en la información afecta la calidad de la edición**. Por 
lo que el mantenimiento de la información no solo es una tarea secundaria o 
«técnica», sino una responsabilidad editorial.

## Octavo elemento metodológico

Ante estos inconvenientes, la solución es el uso de un
**control de versiones**. Una manera práctica de entender qué es esto es
mediante una analogía con los videojuegos:

* En un videojuego el usuario **no** guarda todo el juego según vaya avanzando.
* En un videojuego se guarda el progreso del usuario con puntos de guardado.
* En un videojuego el usuario puede elegir regresar a cualquiera de los puntos.

Entonces, en un control de versiones:

* El usuario **no** guarda continuamente todo el proyecto según vaya avanzando.
* El usuario guarda su trabajo según puntos de control: los *commits*.
* El usuario puede elegir regresar a cualquier punto.

Esto tiene las siguientes ventajas:

1. El espacio necesario disminuye.
2. No existe confusión entre el proyecto actual y los respaldos».
3. Existe un control de los cambios.
4. El trabajo independiente o remoto no queda limitado.
5. Es posible ver los cambios específicos.
6. Existe la opción de tener desarrollos paralelos al proyecto principal
   (*branches*).

![Ejemplo de distintas ramas en un mismo proyecto.](../img/s09_img02.jpg)

El *software* utilizado para este control se llama git. Véase 
[un ejemplo](https://github.com/NikaZhenya/Pecas/commits/master) de cómo pueden
verse su control de cambios en un repositorio.

> ¿Qué es un repositorio? Un repositorio, depósito o archivo es un sitio 
> centralizado donde se almacena y mantiene información digital, habitualmente 
> bases de datos o archivos informáticos. [Más en Wikipedia](https://es.wikipedia.org/wiki/Repositorio).

![¿Qué hace falta para que uses Git?](../img/s09_img03.jpg)

## Noveno elemento metodológico

Con el uso de git se hace posible alojar el repositorio en un servidor
donde cualquier usuario autorizado puede descargarlo y subir o bajar cambios
(*push* y *pull*).

![Esquema de interacción con repositorios a nivel local y con el servidor.](../img/s09_img04.jpg)

Con esto, el *single source publishing* pasa a ser 
*single source and online publishing* ([SSOP]{.versalita}), donde la 
«revolución» digital no va en contra de los conocimientos adquiridos durante 
siglos, sino que revisita, depura o elimina técnicas y metodologías que no son 
las más pertinentes para llevar a cabo una tarea. 

En la edición, la «revolución» digital es la «apertura» del «gremio» al uso de 
las tecnologías de la información en pos de técnicas más aptas pero sin olvidar 
lo que hace de una obra un «buen» libro.

</section>
